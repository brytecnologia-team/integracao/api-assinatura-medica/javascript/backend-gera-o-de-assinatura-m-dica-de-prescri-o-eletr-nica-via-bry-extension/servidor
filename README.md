**Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via BRy Extension**

O lado do servidor foi confeccionado em node.js. Esse serviço simula um servidor local e, a partir dele, são realizadas as chamadas para o BRy HUB.

O arquivo “index.js” habilita as rotas para inicializar e finalizar as assinaturas, que são utilizadas pela aplicação cliente. 

No arquivo .env, existe a necessidade de customização da tag <ACCESS_TOKEN> com o valor de:
* Token JWT em Minhas Aplicações em uma conta pessoal.
* Token JWT em Aplicações da Empresa em uma conta corporativa.

Para executar o exemplo, são necessários dois passos:

1.	Instalar dependências através do comando “npm install”;
2.	Executar o servidor com o comando “node index”.

O servidor estará executando na porta 8002.


### Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  
